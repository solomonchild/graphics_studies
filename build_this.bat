@echo off
REM call inject_gcc.bat
if NOT EXIST bin (
    md bin
)
pushd bin
cmake -G "Ninja" -DCMAKE_BUILD_TYPE=Debug ../ || goto quit
cmake --build . || goto quit
study.exe
:quit
popd

