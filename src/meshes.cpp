#include "meshes.hpp"
#include <bandana/log.hpp>

static void tetr(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, Mesh::Vertices& triangles) {
    triangles.push_back(a);
    triangles.push_back(b);
    triangles.push_back(c);

    triangles.push_back(a);
    triangles.push_back(d);
    triangles.push_back(b);

    triangles.push_back(b);
    triangles.push_back(d);
    triangles.push_back(c);

    triangles.push_back(a);
    triangles.push_back(c);
    triangles.push_back(d);
}


static void divide_tetr(glm::vec3 a, glm::vec3 b, glm::vec3 c, glm::vec3 d, int k, Mesh::Vertices& triangles) {
    if(k > 0) {
        glm::vec3 ab = (a+b)*0.5f;
        glm::vec3 bd = (b+d)*0.5f;
        glm::vec3 da = (a+d)*0.5f;
        glm::vec3 ac = (a+c)*0.5f;
        glm::vec3 bc = (b+c)*0.5f;
        glm::vec3 cd = (c+d)*0.5f;

        divide_tetr(a, ab, ac, da, k-1, triangles);
        divide_tetr(b, bc, bd, ab, k-1, triangles);
        divide_tetr(d, da, bd, cd, k-1, triangles);
        divide_tetr(c, cd, bc, ac, k-1, triangles);

    } else tetr(a,b,c,d, triangles);
}


Mesh::Vertices get_mesh_serp(int k) {
    Mesh::Vertices triangles;
    divide_tetr(
        {-1.0,-1.0, 0.0},
        {0.0,1.0, -0.5},
        {0.0,-1.0, -1.0},
        {1.0,-1.0, 0.0},
        k, triangles
    );

#if 0
    std::ofstream of("3d_serp.mesh");
    for(auto i : triangles) {
        of << i.x << "," << i.y << "," << i.z << ",\n";
    }
#endif
    return triangles;
}

static void triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c, Mesh::Vertices& triangles) {
    triangles.push_back(a);
    triangles.push_back(b);
    triangles.push_back(c);
}

static void divide_triangle(glm::vec3 a, glm::vec3 b, glm::vec3 c, int k, Mesh::Vertices& triangles) {
    if(k > 0) {
        glm::vec3 ab = (a+b)*.5f;
        glm::vec3 ac = (a+c)*.5f;
        glm::vec3 bc = (b+c)*.5f;
        divide_triangle(a, ab, ac, k-1, triangles);
        divide_triangle(b, bc, ab, k-1, triangles);
        divide_triangle(c, ac, bc, k-1, triangles);
    } else triangle(a,b,c, triangles);
}

Mesh::Vertices get_2d_serp(int k) {
    Mesh::Vertices verts;
    divide_triangle(
        {-1, -1, 0}, {0, 1, 0}, {1, -1, 0},
        k, verts
    );
    return verts;
}

constexpr static auto DegToRads = 1.0f/180.0f*3.1415f;
Mesh::Vertices get_sphere() {
    Mesh::Vertices ret;
    for(float phi = -100.0; phi <= 80.0; phi += 20.0f) {
        float phir  = phi*DegToRads;
        float phir20  = (phi+20.0f)*DegToRads;

        for(float theta = -180.0f; theta <= 180.0f; theta += 20.0f) {
            float thetar = theta*DegToRads;
            ret.emplace_back(sin(thetar)*cos(phir), 
                cos(thetar)*cos(phir), 
                sin(phir));
            ret.emplace_back(sin(thetar)*cos(phir20), 
                cos(thetar)*cos(phir20), 
                sin(phir20));
        }
    }

    for(float phi = -100.0; phi <= 80.0; phi += 20.0f) {
        float phir  = phi*DegToRads;
        for(float theta = -180.0f; theta <= 180.0f; theta += 20.0f) {
            float thetar = theta*DegToRads;
            float thetar20 = (theta+20)*DegToRads;
            ret.emplace_back(sin(thetar)*cos(phir), 
                cos(thetar)*cos(phir), 
                sin(phir));

            ret.emplace_back(sin(thetar20)*cos(phir), 
                cos(thetar20)*cos(phir), 
                sin(phir));
        }
    }

    return ret;
}