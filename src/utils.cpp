#include "utils.hpp"

#include <cassert>
#include <fstream>
#include <regex>

#include <glm/gtc/matrix_transform.hpp>

#include <bandana/log.hpp>

namespace Utils {

std::string strip(std::string what) {
    const std::string delims = " \t\n";
    while(delims.find_first_of(*what.begin()) != std::string::npos) {
        what.erase(what.begin());
    }
    if(!what.empty()) {
        while(delims.find_first_of(*what.rbegin()) != std::string::npos) { what.erase(what.end() - 1); }
    }
    return what;
}

Strings split(const std::string& what, const std::string& delim) {
    Strings strs;
    auto start = 0u;
    auto idx = what.find_first_of(delim, start);
    while(idx != std::string::npos) {
        if(auto s = strip(what.substr(start, idx - start)); !s.empty()) {
            strs.push_back(s);
        }
        start = idx + 1;
        idx = what.find_first_of(delim, start);
    }
    return strs;
}



}