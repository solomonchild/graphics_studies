#include "glad/glad.h"
#include "mesh.hpp"

#include <cassert>
#include <fstream>
#include <regex>
#include <tuple>

#include <bandana/log.hpp>

#include "utils.hpp"

using namespace Utils;

void Mesh::init() {
    camera_ = glm::mat4(1.0f);
    proj_ = glm::mat4(1.0f);
    model_ = glm::mat4(1.0f);
    glGenVertexArrays(1, &vao_);
    glBindVertexArray(vao_);
        glGenBuffers(1, &vbo_);
        glGenBuffers(1, &vbo_indices_);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_);
            glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(glm::vec3), vertices_.data(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
            glEnableVertexAttribArray(0);
        if(!indices_.empty()) { 
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_indices_);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices_.size()*sizeof(unsigned int), indices_.data(), GL_STATIC_DRAW);
        }
        glBindVertexArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

static auto read(const std::string& what) {
    std::ifstream triangle_mesh_f(what);
    std::string line;
    assert(triangle_mesh_f);

    auto parse_line = [](const std::string line, const std::regex re, auto& container, const std::string& log_tag) {
    };
    const std::regex re_verts(R"raw((-?\d(?:\.\d+)?), *(-?\d(?:\.\d+)?), *(-?\d(?:\.\d+)?),?)raw");
    const std::regex re_indices(R"raw((\d),?)raw");
    Mesh::Vertices triangle_verts;
    Mesh::Indices triangle_indices;
    bool indices{};
    while(std::getline(triangle_mesh_f, line)) {
        //bandana::log("{}", line);
        if(line == "Indices") {
            indices = true;
            continue;
        }
        else {
            if(!indices) {

                std::sregex_iterator b(line.begin(), line.end(), re_verts);
                std::sregex_iterator e{};
                for(auto it = b; it != e; ++it) {
                    auto x = std::stof(strip((*it)[1].str()));
                    auto y = std::stof(strip((*it)[2].str()));
                    auto z = std::stof(strip((*it)[3].str()));
                    //std::cout << x << y << z << "\n";
                    triangle_verts.push_back({x, y, z});
                }
            }
            else {
                std::sregex_iterator b(line.begin(), line.end(), re_indices);
                std::sregex_iterator e{};
                for(auto it = b; it != e; ++it) {
                    auto x = std::stof(strip((*it)[1].str()));
                    triangle_indices.push_back(x);
                }
            }
        }
    }
    return std::pair(triangle_verts, triangle_indices);
}
Mesh::Mesh(const std::string& fname, Shader::Ptr shader, int type) 
: shader_(shader),
type_(type)
{
    std::tie(vertices_, indices_) = read(fname);
    init();
}

Mesh::Mesh() 
: Mesh(Vertices{}, nullptr)
{
}

Mesh::Mesh(const Vertices& vertices, Shader::Ptr shader, int type) 
: Mesh(vertices, {}, shader, type)
{
}

Mesh::Mesh(const Vertices& vertices, const Indices& indices, Shader::Ptr shader, int type) 
: vertices_(vertices),
  indices_(indices),
  shader_(shader),
  type_(type)
{
    if(vertices.empty()) return;
    init();
}

Mesh::Mesh(Mesh&& m) {
    *this = std::move(m);
}

Mesh& Mesh::operator=(Mesh&& m) {
    std::swap(vertices_, m.vertices_);
    std::swap(indices_, m.indices_);
    std::swap(vao_, m.vao_);
    std::swap(vbo_, m.vbo_);
    std::swap(vbo_indices_, m.vbo_indices_);
    std::swap(shader_, m.shader_);
    std::swap(model_, m.model_);
    std::swap(proj_, m.proj_);
    std::swap(camera_, m.camera_);
    return *this;
}

Mesh::~Mesh() {
    //bandana::loge("Destroyed vbo {} vbo_indices {} vao {} [0x{}]", vbo_, vbo_indices_, vao_, this);
    glDeleteVertexArrays(1, &vao_);
    glDeleteBuffers(1, &vbo_);
    glDeleteBuffers(1, &vbo_indices_);
}

void Mesh::set_transform(glm::mat4 transform) {
    model_ = transform;
}

void Mesh::draw() {
    if(shader_) {
        shader_->use();
        shader_->set(proj_, "proj");
        shader_->set(model_, "model");
        shader_->set(camera_, "camera");
    }
    glBindVertexArray(vao_);
    if(!indices_.empty()) glDrawElements(type_, indices_.size(), GL_UNSIGNED_INT, 0);
    //TODO: lines customize
    else glDrawArrays(type_, 0, vertices_.size());
    //TODO: POINTS/LINES
    glBindVertexArray(0);
    glUseProgram(0);
    //TODO: push it to Shader
}