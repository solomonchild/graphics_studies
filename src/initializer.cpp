#include "initializer.hpp"

#include <bandana/log.hpp>

#include <cassert>

static void enable_stuff() {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);  
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glFrontFace(GL_CCW);
}

static void resize_cbk(GLFWwindow* window, int width, int height) {
    glfwSetWindowAspectRatio(window, width, height);
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
}

GLFWwindow* Initializer::init(int width, int height, ErrorCbk error_cbk, KeyCbk key_cbk, MouseCbk mouse_cbk) {
    if(!glfwInit()) {
        bandana::loge("Couldn't initialize glfw");
        return nullptr;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    auto window = glfwCreateWindow(width, height, "", nullptr, nullptr);
    if(!window) {
        bandana::loge("Window creation failed");
        assert(false);
    }
    glfwMaximizeWindow(window);
    if(key_cbk) glfwSetKeyCallback(window, key_cbk);
    if(error_cbk) glfwSetErrorCallback(error_cbk);
    if(mouse_cbk) glfwSetCursorPosCallback(window, mouse_cbk);
    glfwSetWindowSizeCallback(window, resize_cbk);

    glfwMakeContextCurrent(window);
    if(!gladLoadGLLoader(GLADloadproc(glfwGetProcAddress))) { 
        bandana::loge("failed to initialize GLAD"); 
        assert(false);
    }
    enable_stuff();
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);
    return window;
}