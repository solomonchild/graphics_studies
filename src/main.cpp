#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <array>
#include <chrono>
#include <cmath>
#include <fstream>
#include <random>
#include <set>
#include <vector>

#include <bandana/log.hpp>

#include "initializer.hpp"
#include "mesh.hpp"
#include "shader.hpp"
#include "utils.hpp"
#include "meshes.hpp"

using namespace Utils;

int WIDTH = 640;
int HEIGHT = 480;
void error_cbk(int code, const char* descr) {
    bandana::loge("Error from GLFW: {} ({})", code, descr);
}

using MeshPtr = std::shared_ptr<Mesh>;
Shader::Ptr console_shader;
MeshPtr console_mesh; 
bool draw{};
std::unordered_map<std::string, MeshPtr> meshes;

std::array<glm::vec3, 2> camera_pos {{{0.0f, 0.0f, 3.0f}, {+5.0f, 0.0f, 3.0f}}};
std::array<glm::vec3, 2> camera_targets {{{0.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 3.0f}}};
std::array<int, 2> camera_type {{1, 1}};
static_assert(camera_pos.size() == camera_targets.size());
int cameraIdx{};
std::array<glm::mat4, 2> projectionMatrices;

void update(GLFWwindow* window) {

    if(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    static auto last_update = std::chrono::system_clock::now();
    auto delta = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - last_update).count()/1000.0f;
    
    constexpr static float SENS = 3.0;
    float speed = SENS * delta;
    if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) != GLFW_RELEASE) {
        speed *= 10;
    }
    if(glfwGetKey(window, GLFW_KEY_W) != GLFW_RELEASE) {
        camera_pos[cameraIdx] += speed*camera_targets[cameraIdx];
    }
    if(glfwGetKey(window, GLFW_KEY_S) != GLFW_RELEASE) {
        camera_pos[cameraIdx] -= speed*camera_targets[cameraIdx];
    }
    if(glfwGetKey(window, GLFW_KEY_D) != GLFW_RELEASE) {
        camera_pos[cameraIdx] += speed*glm::cross(camera_targets[cameraIdx], glm::vec3(0.0f, 1.0f, 0.0f));
    }
    if(glfwGetKey(window, GLFW_KEY_A) != GLFW_RELEASE) {
        camera_pos[cameraIdx] -= speed*glm::cross(camera_targets[cameraIdx], glm::vec3(0.0f, 1.0f, 0.0f));
    }
    last_update = std::chrono::system_clock::now();

}

double last_x = WIDTH/2.0;
double last_y = HEIGHT/2.0;
float pitch = 0.0f;
float yaw = -90.0f;
bool right_button_pressed{};
bool left_button_pressed{};
void mouse_cbk(GLFWwindow* window, double xpos, double ypos) {
    if(!right_button_pressed) return;
    constexpr double SENS = 0.1;
    static bool init{};
    if(!init) {
        last_x = xpos;
        last_y = ypos;
        init = true;
    }

    auto delta_x = -(last_x - xpos);
    auto delta_y = (last_y - ypos);
    last_x = xpos;
    last_y = ypos;
    if(left_button_pressed && right_button_pressed) {
        camera_pos[cameraIdx].y += delta_y * SENS/5;
        return;
    }

    yaw += (delta_x * SENS);
    pitch += (delta_y * SENS);

    pitch = std::clamp(pitch, -89.0f, 89.0f);
    yaw = glm::mod(yaw, 360.0f);

    glm::vec3 dir;
    dir.x = std::cos(glm::radians(pitch))*std::cos(glm::radians(yaw));
    dir.y = std::sin(glm::radians(pitch));
    dir.z = std::cos(glm::radians(pitch))*std::sin(glm::radians(yaw));
    camera_targets[cameraIdx] = glm::normalize(dir);
}

void mouse_btn_cbk(GLFWwindow* window, int button, int action,int) {
    if(button == GLFW_MOUSE_BUTTON_1) {
        left_button_pressed = (action == GLFW_PRESS);
    }
    if(button == GLFW_MOUSE_BUTTON_2) {
        right_button_pressed = (action == GLFW_PRESS);
        glfwSetInputMode(window, GLFW_CURSOR, right_button_pressed?GLFW_CURSOR_DISABLED:GLFW_CURSOR_NORMAL);
        glfwGetCursorPos (window, &last_x, &last_y);
    }
}

void setupCamerasUI() {
    static const char* items[] {
        "camera 1", "camera 2"
    };
    ImGui::Combo("Camera", &cameraIdx, items, camera_pos.size());
}

void processProjectionSelection() {
            const char* items[] = {"Ortho", "Perspective"};
            constexpr static int ORTHO = 0;
            ImGui::Combo("Projection type", &camera_type[cameraIdx], items, 2);
            if(camera_type[cameraIdx] == ORTHO) {
                ImGui::Text("Orthographic camera settings");
                static float l{-1},r{1},b{-1},t{1},n{0.01},f{10};
                ImGui::PushItemWidth(100);
                    ImGui::DragFloat("Left", &l);
                    ImGui::DragFloat("Right", &r);
                    ImGui::DragFloat("Bottom", &b);
                    ImGui::DragFloat("Top", &t);
                    ImGui::DragFloat("Near", &n);
                    ImGui::DragFloat("Far", &f);
                ImGui::PopItemWidth();
                projectionMatrices[cameraIdx] = glm::ortho(l, r, b, t, n, f);
            } else {
                ImGui::Text("Perspective camera settings");
                static float fovy{45.0f}, near{0.1}, far{100};
                static int w{WIDTH}, h{HEIGHT}; 
                ImGui::PushItemWidth(100);
                ImGui::DragInt("Width", &w); 
                ImGui::DragInt("Height", &h);
                ImGui::DragFloat("Near", &near, 0.1f); 
                ImGui::DragFloat("Far", &far);
                ImGui::SliderFloat("FOVy", &fovy, 0.0f, 180.0f);
                ImGui::PopItemWidth();
                projectionMatrices[cameraIdx]  = glm::perspective(glm::radians(fovy), w/float(h), near, far);
            }
}



static glm::mat4 getCamera(int idx) {
    idx = std::clamp<int>(idx, 0, camera_pos.size());
    return glm::lookAt(
        camera_pos[idx],
        camera_pos[idx] + camera_targets[idx],
        glm::vec3(0.0f, 1.0f, 0.0f)
    );
}

static bool show_demo_window = false;
static bool serp = true;
static glm::vec4 clear_color  {0.0f, 0.0f, 0.0f, 1.0f};
static bool sphere = true;

float twistX{};
float twistY{};
float twistZ{};

glm::vec3 line_start = {.0f, -.5f, -1.0f};
glm::vec3 line_end = {.0f, -0.5f, 0.0f};
bool line_draw{};
float angle = 10.0f;

void UI() {
    ImGui::Begin("Settings");                
        ImGui::Checkbox("Load Serpinski gasket (3d)", &serp);
        ImGui::DragFloat("Twist gasket about X", &twistX, 0.005f, -50.0f, 50.0f); 
        ImGui::DragFloat("Twist gasket about Y", &twistY, 0.005f, -50.0f, 50.0f); 
        ImGui::DragFloat("Twist gasket about Z", &twistZ, 0.005f, -50.0f, 50.0f); 
        ImGui::Checkbox("Render a sphere", &sphere);
        ImGui::Checkbox("Show demo window.", &show_demo_window);
        ImGui::DragFloat4("Clear color", &clear_color.x, 0.05f, 0.0f, 1.0f);
        setupCamerasUI();
        ImGui::DragFloat3("Camera position", &camera_pos[cameraIdx].x, 0.05f, -50.0f, 50.0f); 
        ImGui::DragFloat3("Camera target", &camera_targets[cameraIdx].x, 0.05f, -50.0f, 50.0f); 
        ImGui::Checkbox("Draw line?", &line_draw); 
        ImGui::DragFloat3("Line start", &line_start.x, 0.01f, -1.0f, 1.0f); 
        ImGui::DragFloat3("Line end", &line_end.x, 0.01f, -1.0f, 1.0f); 
        ImGui::DragFloat("Angle", &angle, 0.10f, -180.0f, 180.0f); 
        processProjectionSelection();
    ImGui::End();
}

auto getCameraFrustum(int idx) {
        auto camera = getCamera(idx);
        auto viewMatrix = glm::inverse(camera);
        auto invProj = viewMatrix*glm::inverse(projectionMatrices[idx]);
        auto vvvs = unitCube;
        std::transform(unitCube.begin(), unitCube.end(), vvvs.begin(), [invProj] (const glm::vec3& v){
            glm::vec4 t(v, 1.0f);
            t = invProj * t;
            return glm::vec3{t.x/t.w, t.y/t.w, t.z/t.w};
        });
        auto frustum = std::make_shared<Mesh>(
        vvvs,
        Mesh::Indices{0, 1, 1, 2, 2, 3, 3, 0,
                    4, 5, 5, 6, 6, 7, 7, 4,
                    0,4, 1,5, 2,6, 3,7,
                    4,6, 5,7
        },
        std::make_shared<Shader>("line.frag", "shader.vert"), GL_LINES);
        frustum->set_camera(glm::mat4(1));
        frustum->set_proj(glm::mat4(1));
        return frustum;
}

int main() {
    //TODO: employ RAII for termination
    Finally f1([]{glfwTerminate();});

    auto window = Initializer::init(WIDTH, HEIGHT, error_cbk, nullptr, mouse_cbk);
    glfwGetFramebufferSize(window, &WIDTH, &HEIGHT);
    //TODO: move to init
    glfwSetMouseButtonCallback(window, mouse_btn_cbk);
    if(!window) {
        bandana::loge("Could not create window");
        return -1;
    }
    glfwSetCursorPos (window, last_x, last_y);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;

    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 130");

    //TODO: employ RAII for destroying window
    Finally f2([&]{
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
        glfwDestroyWindow(window);
    });

    meshes["tetr"] = std::make_shared<Mesh>("2d_triangle.mesh", std::make_shared<Shader>("shader.frag", "shader.vert"));

    glm::mat4 model(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, 0.0f, -2.0f));
    model = glm::scale(model, glm::vec3(0.5f, 0.5f, 1.0f));
    meshes["tetr"]->set_model(model);

    auto pos = camera_pos[cameraIdx];
    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        auto camera = getCamera(cameraIdx);
        auto viewMatrix = glm::inverse(camera);
        if(line_draw) {
            meshes["line"] = std::make_shared<Mesh>(
                Mesh::Vertices {
                    line_start,
                    line_end
                }, std::make_shared<Shader>("white.frag", "shader.vert"), GL_LINES);
            meshes["line"]->set_model(glm::rotate(viewMatrix, glm::radians(angle), glm::vec3(1.0f, 0.0, 0.0)));
        } else {
            meshes.erase("line");
        }

        for(auto [name, mesh] : meshes) {
            if(!mesh) continue;
            mesh->set_camera(camera);
            mesh->set_proj(projectionMatrices[cameraIdx]);
            mesh->draw();
        }
        std::shared_ptr<Mesh> sh;
        {
            auto otherIdx = 0;
            if(cameraIdx == 0) otherIdx = 1;
            if(line_draw) {
                meshes["line"]->set_model(glm::rotate(glm::inverse(getCamera(otherIdx)), glm::radians(angle), glm::vec3(1.0, 0.0, 0.0)));
                meshes["line"]->draw();
            }

            sh = getCameraFrustum(otherIdx);
            sh->set_camera(camera);
            sh->set_proj(projectionMatrices[otherIdx]);
            sh->draw();
        }

        if(sphere) {
            if(meshes.find("sphere") == meshes.end()) {
                meshes["sphere"] = std::make_shared<Mesh>(get_sphere(), std::make_shared<Shader>("white.frag", "shader.vert"), GL_LINES);
                meshes["sphere"]->set_model(glm::translate(glm::mat4{1.0f}, glm::vec3(0.0f, .0f, +5.0f)));
            }
        } else {
            meshes.erase("sphere");
        }

        if (show_demo_window)
            ImGui::ShowDemoWindow(&show_demo_window);
        if(serp) {
            //model = glm::rotate(model, 3.1415f/4*twist, glm::vec3(0.0f,.0f,1.0f));
            if(meshes.find("serp2d") == meshes.end()) {
                model = glm::translate(model, glm::vec3(0.0f, .0f, -3.0f));
                meshes["serp2d"] = std::make_shared<Mesh>(get_2d_serp(), std::make_shared<Shader>("line.frag", "shader.vert"));
            }
            meshes["serp2d"]->set_model(model);
            meshes["serp2d"]->shader()->use();
            meshes["serp2d"]->shader()->set(twistX, "twistX");
            meshes["serp2d"]->shader()->set(twistY, "twistY");
            meshes["serp2d"]->shader()->set(twistZ, "twistZ");

            if(meshes.find("serp3d") == meshes.end()) {
                meshes["serp3d"] = std::make_shared<Mesh>(get_mesh_serp(), std::make_shared<Shader>("line.frag", "shader.vert"));
                meshes["serp3d"]->set_model(glm::translate(model, glm::vec3(+3.0f, .0f, -3.0f)));
            }
        } else {
            meshes.erase("serp2d");
            meshes.erase("serp3d");
        }
        UI();

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(window);
        update(window);
    }
}
