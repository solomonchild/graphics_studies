#include "shader.hpp"

#include <cassert>
#include <fstream>
#include <vector>

#include <glad/glad.h>
#include <glm/gtc/type_ptr.hpp>

#include <bandana/log.hpp>


static std::string get_contents(const std::string& fname) {
    std::ifstream file(fname);
    assert(file && ("Cannot open " + fname).c_str());
    std::string line;
    std::string res;
    while(std::getline(file, line)) {
        res += line + "\n\r";
    }
    return res;
}

static int attach_shader(int program, const std::string& fname, int type) {
    auto shader = glCreateShader(type);
    auto source = get_contents(fname);
    const char* sources[] = {source.c_str()};
    const int lens[] = {static_cast<int>(source.size())};
    glShaderSource(shader, 1, sources, lens);
    glCompileShader(shader);
    auto status = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE) {
        auto max_len = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &max_len);
        std::vector<char> error(max_len);
        glGetShaderInfoLog(shader, max_len, &max_len, error.data());
        std::string str(error.begin(), error.end());
        bandana::loge("Error compiling shader {}", str.c_str());
        assert(false);
        glDeleteShader(shader);
    }
    glAttachShader(program, shader);
    return shader;
}

void Shader::set(float m, const std::string& what) {
    auto loc = glGetUniformLocation(program_, what.c_str());
    if(loc < 0 ) {
        bandana::loge("Cannot find uniform {}, {}", what.c_str(), loc);
        return;
    }
    glUniform1f(loc, m);
}

void Shader::set(const glm::mat4& m, const std::string& what) {
    auto loc = glGetUniformLocation(program_, what.c_str());
    if(loc < 0 ) {
        bandana::loge("Cannot find uniform {}, {}", what.c_str(), loc);
        return;
    }
    glUniformMatrix4fv(loc, 1, false, glm::value_ptr(m));
}
void Shader::use() {
    glUseProgram(program_);
}

Shader::Shader(const std::string& frag_name, const std::string& vert_name) {
    program_ = glCreateProgram();
    auto vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    auto frag = attach_shader(program_, frag_name, GL_FRAGMENT_SHADER);
    auto vert = attach_shader(program_, vert_name, GL_VERTEX_SHADER);
    glLinkProgram(program_);

    auto status = 0;
    glGetProgramiv(program_, GL_LINK_STATUS, &status);
    if(status != GL_TRUE) {
        auto max_len = 0;
        glGetProgramiv(program_, GL_INFO_LOG_LENGTH, &max_len);
        std::vector<char> error(max_len);
        glGetProgramInfoLog(program_, max_len, &max_len, error.data());
        std::string str(error.begin(), error.end());
        bandana::loge("Error compiling shader {}", str.c_str());
        assert(false);
    }
    glValidateProgram(program_);
    glGetProgramiv(program_, GL_VALIDATE_STATUS, &status);
    if(status != GL_TRUE) {
        auto max_len = 0;
        glGetProgramiv(program_, GL_INFO_LOG_LENGTH, &max_len);
        std::vector<char> error(max_len);
        glGetProgramInfoLog(program_, max_len, &max_len, error.data());
        std::string str(error.begin(), error.end());
        bandana::loge("Error validating a shader {}", str.c_str());

    }
    glUseProgram(program_);
}