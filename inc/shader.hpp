#pragma once

#include <string>
#include <memory>
#include <glm/glm.hpp>

class Shader {
    public:
        using Ptr = std::shared_ptr<Shader>;
        Shader(const std::string& frag, const std::string& vert);
        void set(const glm::mat4& m, const std::string& what);
        void set(float m, const std::string& what);
        void use();
    private:
        int program_;
};