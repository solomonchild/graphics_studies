#pragma once

#include <functional>
#include <string>
#include <glm/glm.hpp>

namespace Utils {
using Strings = std::vector<std::string>;

std::string strip(std::string what);

Strings split(const std::string& what, const std::string& delim = " ");

struct Finally { 
    using Fn = std::function<void(void)>;
    Finally(Fn&& fn) 
        :f(fn)
    { }

    ~Finally() {
        f();
    }

    Fn f;
};

}