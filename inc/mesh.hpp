#pragma once

#include <gl/GL.h>
#include <glm/glm.hpp>
#include <string>
#include <vector>

#include "shader.hpp"

class Mesh final {
public:
    using Vertices = std::vector<glm::vec3>;
    using Indices = std::vector<GLuint>;
    Mesh();
    Mesh(const std::string& fname, Shader::Ptr shader, int type = GL_TRIANGLES);
    Mesh(Mesh&&);
    Mesh(const Vertices& vertices, Shader::Ptr shader, int type = GL_TRIANGLES);
    Mesh(const Vertices&, const Indices&, Shader::Ptr shader, int type = GL_TRIANGLES);
    Mesh& operator=(Mesh&&);
    ~Mesh();
    void set_camera(const glm::mat4& cam)
    {
        camera_ = cam;
    }
    void set_proj(const glm::mat4& proj)
    {
        proj_ = proj;
    }
    void set_model(const glm::mat4& model)
    {
        model_ = model;
    }

    void set_vertices(Vertices verts) {
        vertices_ = std::move(verts);
        init();
    }

    void init();
    void draw();
    void set_transform(glm::mat4 transform);
    Shader::Ptr shader() { return shader_; }
private:
    Vertices vertices_;
    Indices indices_;
    GLuint vao_;
    GLuint vbo_;
    GLuint vbo_indices_;
    Shader::Ptr shader_;
    glm::mat4 proj_;
    glm::mat4 model_;
    glm::mat4 camera_;
    int type_ = GL_TRIANGLES;
};