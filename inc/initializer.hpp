#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

class Initializer {
public:
    using ErrorCbk = void (*)(int,const char*);
    using KeyCbk = void (*)(GLFWwindow*, int, int, int, int);
    using MouseCbk = void(*)(GLFWwindow*, double, double);
    static GLFWwindow* init(int width, int height, ErrorCbk ecbk = nullptr, KeyCbk kcbk = nullptr, MouseCbk mcbk = nullptr);
};