#include <glm/glm.hpp>
#include <vector>
#include "mesh.hpp"

Mesh::Vertices get_mesh_serp(int k = 5);

Mesh::Vertices get_2d_serp(int k = 5);

const static Mesh::Vertices unitCube {
    {-1.0f, -1.0f, +1.0f},
    {-1.0f, +1.0f, +1.0f},
    {+1.0f, +1.0f, +1.0f},
    {+1.0f, -1.0f, +1.0f},

    {-1.0f, -1.0f, -1.0f},
    {-1.0f, +1.0f, -1.0f},
    {+1.0f, +1.0f, -1.0f},
    {+1.0f, -1.0f, -1.0f},
};

const static Mesh::Vertices arrow {
    {0.0f, 0.5f, 0.0f},
    {1.0f, 0.5f, 0.0f},

    {1.0f, 0.5f, 0.0f},
    {0.8f, 1.0f, 0.0f},

    {1.0f, 0.5f, 0.0f},
    {0.8f, 0.0f, 0.0f},
};

Mesh::Vertices get_sphere();