#version 330
in vec3 in_pos;
uniform mat4 model;
uniform mat4 proj;
uniform mat4 camera;
uniform float twistX;
uniform float twistY;
uniform float twistZ;
out vec4 color;
out vec4 pos;

mat4 rotZ(float inAngle, vec3 pos) {
    float dist = sqrt(pos.x*pos.x+pos.y*pos.y+pos.z*pos.z);
    float angle = inAngle*dist;
    return mat4 (
        cos(angle), -sin(angle), 0, 0,
        sin(angle), cos(angle), 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1
    );
}

mat4 rotX(float inAngle, vec3 pos) {
    float dist = sqrt(pos.x*pos.x+pos.y*pos.y+pos.z*pos.z);
    float angle = inAngle*dist;
    return mat4 (
        1, 0, 0, 0,
        0, cos(angle), -sin(angle), 0,
        0, sin(angle), cos(angle), 0,
        0, 0, 0, 1
    );
}

mat4 rotY(float inAngle, vec3 pos) {
    float dist = sqrt(pos.x*pos.x+pos.y*pos.y+pos.z*pos.z);
    float angle = inAngle*dist;
    return mat4 (
        cos(angle), 0, sin(angle), 0,
        0, 1, 0, 0,
        -sin(angle), 0, cos(angle), 0,
        0, 0, 0, 1
    );
}

void main() {
    mat4 rX = rotX(twistX, in_pos);
    mat4 rY = rotY(twistY, in_pos);
    mat4 rZ = rotZ(twistZ, in_pos);
    mat4 new_model = model*rX*rY*rZ;
    gl_Position = proj*camera*new_model*vec4(in_pos, 1);
    color = vec4(clamp(in_pos, 0.0f, 1.0f), 1);
    pos = vec4(in_pos, 1);
}
