#version 330

in vec4 color;
in vec4 pos;
out vec4 outColor;

void main() {
    outColor = vec4((1.0+pos.z)/2.0, (1.0+pos.z)/2.0, (1.0+pos.z)/2.0, 1.0);
}

