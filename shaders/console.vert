#version 330
in vec3 in_pos;
uniform mat4 model;
uniform mat4 proj;
uniform mat4 camera;
out vec4 color;

void main() {
    gl_Position = proj*camera*model*vec4(in_pos, 1);
    color = vec4(0.0f, 0.0f, 0.0f, 0.7f);
}
